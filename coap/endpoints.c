
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "coap.h"

char light = '0';

const uint16_t well_known_response_len = 1500;
static char rsp[1500] = "";
void build_well_known_response(void);

void endpoint_setup(void)
{
    build_well_known_response();
}

const coap_endpoint_path_t path_well_known_core = {2, {".well-known", "core"}};
int handle_get_well_known_core(coap_rw_buffer_t *rw_buf,
                                      const coap_packet_t *inpkt, coap_packet_t *outpkt,
                                      uint8_t id_hi, uint8_t id_lo)
{
    return coap_make_response(rw_buf, outpkt,
                             (const uint8_t *)rsp, strlen(rsp),
                             id_hi, id_lo,
                             &inpkt->tok,
                             COAP_RSPCODE_CONTENT,
                             COAP_CONTENTTYPE_APPLICATION_LINKFORMAT);
}

const coap_endpoint_path_t path_light = {1, {"light"}};
int handle_get_light(coap_rw_buffer_t *rw_buf,
                            const coap_packet_t *inpkt, coap_packet_t *outpkt,
                            uint8_t id_hi, uint8_t id_lo)
{
    return coap_make_response(rw_buf, outpkt,
                             (const uint8_t *)&light, 1,    // 负载内容和长度
                             id_hi, id_lo,                  // Message ID
                             &inpkt->tok,                   // CoAP ToKen
                             COAP_RSPCODE_CONTENT,          // 2.05 Content
                             COAP_CONTENTTYPE_TEXT_PLAIN);  // 负载类型文本格式
}

int handle_put_light(coap_rw_buffer_t *rw_buf,
                            const coap_packet_t *inpkt, coap_packet_t *outpkt,
                            uint8_t id_hi, uint8_t id_lo)
{
    if (inpkt->payload.len == 0)
    {
        return coap_make_response(rw_buf, outpkt,
                                  NULL, 0,
                                  id_hi, id_lo,
                                  &inpkt->tok,
                                  COAP_RSPCODE_BAD_REQUEST,
                                  COAP_CONTENTTYPE_TEXT_PLAIN);
    }

    if (inpkt->payload.p[0] == '1')
    {
        light = '1';
        printf("ON\n");
        return coap_make_response(rw_buf, outpkt,
                                (const uint8_t *)&light, 1,
                                id_hi, id_lo, &inpkt->tok,
                                COAP_RSPCODE_CHANGED,
                                COAP_CONTENTTYPE_TEXT_PLAIN);
    }
    else
    {
        light = '0';
        printf("OFF\n");
        return coap_make_response(rw_buf, outpkt,
                                 (const uint8_t *)&light, 1,
                                 id_hi, id_lo, &inpkt->tok,
                                 COAP_RSPCODE_CHANGED,
                                 COAP_CONTENTTYPE_TEXT_PLAIN);
    }
}

const coap_endpoint_path_t path_hello = {1, {"hello"}};
int handle_get_hello(coap_rw_buffer_t *rw_buf,
                     const coap_packet_t *inpkt, coap_packet_t *outpkt,
                     uint8_t id_hi, uint8_t id_lo)
{
    const char hello[] = "Hello CoAP";
    return coap_make_response(rw_buf, outpkt,                           // 负载内容和长度
                             (const uint8_t *)hello, strlen(hello),     // Message ID
                             id_hi, id_lo,
                             &inpkt->tok,                               // CoAP ToKen
                             COAP_RSPCODE_CONTENT,                      // 2.05 Content
                             COAP_CONTENTTYPE_TEXT_PLAIN);
}

const coap_endpoint_path_t path_counter = {1, {"counter"}};
int handle_get_counter(coap_rw_buffer_t *rw_buf,
                     const coap_packet_t *inpkt, coap_packet_t *outpkt,
                     uint8_t id_hi, uint8_t id_lo)
{
    static int counter = 0;
    counter++;

    char str[32];
    sprintf(str, "%d", counter);

    return coap_make_response(rw_buf, outpkt,                           // 负载内容和长度
                             (const uint8_t *)str, strlen(str),     	// Message ID
                             id_hi, id_lo,
                             &inpkt->tok,                               // CoAP ToKen
                             COAP_RSPCODE_CONTENT,                      // 2.05 Content
                             COAP_CONTENTTYPE_TEXT_PLAIN);
}

const coap_endpoint_path_t path_json_counter = {2, {"json", "counter"}};
int handle_get_json_counter(coap_rw_buffer_t *rw_buf,
                                      const coap_packet_t *inpkt, coap_packet_t *outpkt,
                                      uint8_t id_hi, uint8_t id_lo)
{
    static int counter = 0;
    counter++;
    char str[48];
    sprintf(str, "{\"value\":%d}", counter);

    return coap_make_response(rw_buf, outpkt,
                             (const uint8_t *)str, strlen(str),
                             id_hi, id_lo,
                             &inpkt->tok,
                             COAP_RSPCODE_CONTENT,
                             COAP_CONTENTTYPE_APPLICATION_JSON);
}

const coap_endpoint_path_t path_add = {1, {"add"}};
int handle_put_add(coap_rw_buffer_t *rw_buf, const coap_packet_t *inpkt,
                   coap_packet_t *outpkt, uint8_t id_hi, uint8_t id_lo)
{
    if (inpkt->payload.len == 0)
    {
        return coap_make_response(rw_buf, outpkt,
                                  NULL, 0,
                                  id_hi, id_lo,
                                  &inpkt->tok,
                                  COAP_RSPCODE_BAD_REQUEST,
                                  COAP_CONTENTTYPE_TEXT_PLAIN);
    }

    char str[48];
    memset(str, 0x00, 48);
    memcpy(str, inpkt->payload.p, inpkt->payload.len);
    printf("%s\n", str);
    int a, b, sum;
    int ret;
    ret = sscanf(str, "a=%d&b=%d", &a, &b);
    sum = a + b;
    if (2 == ret)
    {
        printf("a + b = %d", sum);
        memset(str, 0x00, 48);
        sprintf(str, "%d", sum);
        return coap_make_response(rw_buf, outpkt,
                                 (const uint8_t *)str, strlen(str),
                                 id_hi, id_lo, &inpkt->tok,
                                 COAP_RSPCODE_CHANGED,
                                 COAP_CONTENTTYPE_TEXT_PLAIN);
    }
    else
    {
        printf("ERR\n");
        return coap_make_response(rw_buf, outpkt,
                                 (const uint8_t *)"ERR", 3,
                                 id_hi, id_lo, &inpkt->tok,
                                 COAP_RSPCODE_BAD_REQUEST,
                                 COAP_CONTENTTYPE_TEXT_PLAIN);
    }
}

int handle_get_add(coap_rw_buffer_t *rw_buf,
                   const coap_packet_t *inpkt, coap_packet_t *outpkt,
                   uint8_t id_hi, uint8_t id_lo)
{
    int a, b, sum;
    int ret1, ret2;
    char str_query[2][32];
    char str[32];
    const coap_option_t *opt;

    uint8_t counter = 0;;
    // 尝试获得QUERY
    opt = coap_find_options(inpkt, COAP_OPTION_URI_QUERY, &counter);
    printf("query option num: %d\n", counter);

    // 只处理两个QUERY，返回错误响应
    if (counter != 2)
    {
        return coap_make_response(rw_buf, outpkt,
                                 NULL, 0,
                                 id_hi, id_lo, &inpkt->tok,
                                 COAP_RSPCODE_BAD_REQUEST,
                                 COAP_CONTENTTYPE_TEXT_PLAIN);
    }

    for (int i = 0; i < counter; i++)
    {
        // 把option buf中的内容复制到字符串缓冲区中
        coap_buffer_to_string(str_query[i], 32, &(opt[i].buf));
    }

    // 查找 a= 和  b=
    ret1 = sscanf(str_query[0], "a=%d", &a);
    ret2 = sscanf(str_query[1], "b=%d", &b);

    if (1 == ret1 && 1 == ret2)
    {
        sum = a + b;
        printf("sum = %d\n", sum);
        memset(str, 0x00, 32);
        sprintf(str, "%d", sum);
        return coap_make_response(rw_buf, outpkt,                           // 负载内容和长度
                                 (const uint8_t *)str, strlen(str),         // Message ID
                                 id_hi, id_lo,
                                 &inpkt->tok,                               // CoAP ToKen
                                 COAP_RSPCODE_CONTENT,                      // 2.05 Content
                                 COAP_CONTENTTYPE_TEXT_PLAIN);
    }
    return 0;
}

const coap_endpoint_path_t path_stream = {1, {"stream"}};
int handle_get_stream(coap_rw_buffer_t *rw_buf,
                      const coap_packet_t *inpkt, coap_packet_t *outpkt,
                      uint8_t id_hi, uint8_t id_lo)
{
    char str[] = {0xD5, 0xC8};

    return coap_make_response(rw_buf, outpkt,                           // 负载内容和长度
                             (const uint8_t *)str, sizeof(str)/sizeof(str[0]),         // Message ID
                             id_hi, id_lo,
                             &inpkt->tok,                               // CoAP ToKen
                             COAP_RSPCODE_CONTENT,                      // 2.05 Content
                             COAP_CONTENTTYPE_OCTECT_STREAM);
}

const coap_endpoint_path_t path_swap = {1, {"swap"}};
int handle_post_swap(coap_rw_buffer_t *rw_buf,
                     const coap_packet_t *inpkt, coap_packet_t *outpkt,
                     uint8_t id_hi, uint8_t id_lo)
{
    if (inpkt->payload.len == 0)
    {
        return coap_make_response(rw_buf, outpkt,
                                  NULL, 0,
                                  id_hi, id_lo,
                                  &inpkt->tok,
                                  COAP_RSPCODE_BAD_REQUEST,
                                  COAP_CONTENTTYPE_OCTECT_STREAM);
    }

    char str[48];
    char rev_str[48];
    int i = 0;
    int j = 0;

    memset(str, 0x00, 48);
    memcpy(str, inpkt->payload.p, inpkt->payload.len);
    printf("str:%s\n", str);
    //int len = inpkt->payload.len;
    int len = strlen(str);
    printf("len = %d\n", len);
    printf("str:\n");
    for (i = 0; i < len; i++)
    {
        printf("0x%02X ", str[i]);
    }
    printf("\n");
    printf("rev_str:\n");
    for (i = len - 1, j = 0; i >= 0; i--,j++)
    {
        rev_str[j] = str[i];
        printf("0x%02X ", rev_str[j]);
    }

    return coap_make_response(rw_buf, outpkt,
                             (const uint8_t *)&rev_str, strlen(rev_str),
                             id_hi, id_lo, &inpkt->tok,
                             COAP_RSPCODE_CHANGED,
                             COAP_CONTENTTYPE_OCTECT_STREAM);
}

const coap_endpoint_path_t path_sort = {1, {"sort"}};
int handle_post_sort(coap_rw_buffer_t *rw_buf,
                     const coap_packet_t *inpkt, coap_packet_t *outpkt,
                     uint8_t id_hi, uint8_t id_lo)
{
    if (inpkt->payload.len == 0)
    {
        return coap_make_response(rw_buf, outpkt,
                                  NULL, 0,
                                  id_hi, id_lo,
                                  &inpkt->tok,
                                  COAP_RSPCODE_BAD_REQUEST,
                                  COAP_CONTENTTYPE_OCTECT_STREAM);
    }

    char str[48];
    memset(str, 0x00, 48);
    memcpy(str, inpkt->payload.p, inpkt->payload.len);
    //printf("str: %s\n", str);
    int len = inpkt->payload.len;
    printf("%d\n", len);
    int i = 0;
    int j = 0;
    int tmp = 0;
    for (i = 0; i < len; i++)
    {
        printf("0x%02X ", str[i]);
    }
    printf("\n");
    for(i =0 ; i< len-1; ++i)
    {
        for(j = 0; j < len-i-1; j++)
        {
            if(str[j] > str[j+1])
            {
                tmp = str[j];
                str[j] = str[j+1];
                str[j+1] = tmp;
            }
        }
    }
    for (i = 0; i < len; i++)
    {
        printf("0x%02X ", str[i]);
    }
    //printf("str: %s\n", str);
    return coap_make_response(rw_buf, outpkt,
                             (const uint8_t *)&str, strlen(str),
                             id_hi, id_lo, &inpkt->tok,
                             COAP_RSPCODE_CHANGED,
                             COAP_CONTENTTYPE_OCTECT_STREAM);
}

const coap_endpoint_t endpoints[] =
{
    {COAP_METHOD_GET, handle_get_well_known_core, &path_well_known_core, "ct=40"},
    {COAP_METHOD_GET, handle_get_light, &path_light, "ct=0"},
    {COAP_METHOD_PUT, handle_put_light, &path_light, NULL},
    {COAP_METHOD_GET, handle_get_hello, &path_hello, "ct=0"},
    {COAP_METHOD_GET, handle_get_counter, &path_counter, "ct=0"},
    {COAP_METHOD_GET, handle_get_json_counter, &path_json_counter, "ct=50"},
    {COAP_METHOD_PUT, handle_put_add, &path_add, NULL},
    {COAP_METHOD_GET, handle_get_add, &path_add, "ct=0"},
    {COAP_METHOD_GET, handle_get_stream, &path_stream, "ct=0"},
    {COAP_METHOD_POST, handle_post_swap, &path_swap, "ct=42"},
    {COAP_METHOD_POST, handle_post_sort, &path_sort, "ct=42"},
    {(coap_method_t)0, NULL, NULL, NULL}
};

void build_well_known_response(void)
{
    uint16_t len = well_known_response_len;
    const coap_endpoint_t *ep = endpoints;      // endpoinsts为全局变量
    int i;

    len--; // Null-terminated string

    while (NULL != ep->handler)
    {
        if (NULL == ep->core_attr)
        {
            ep++;
            continue;
        }

        if (0 < strlen(rsp))
        {
            strncat(rsp, ",", len);
            len--;
        }

        strncat(rsp, "<", len);
        len--;

        for (i = 0; i < ep->path->count; i++) {
            strncat(rsp, "/", len);
            len--;

            strncat(rsp, ep->path->elems[i], len);
            len -= strlen(ep->path->elems[i]);
        }

        strncat(rsp, ">;", len);
        len -= 2;

        strncat(rsp, ep->core_attr, len);
        len -= strlen(ep->core_attr);

        ep++;
    }
}
