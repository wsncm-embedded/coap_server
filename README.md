# 说明 #

CoAP Server 测试工程

# Windows 工程 #
Eclipse + MinGW工程

通过Windows Socket实现CoAP Server

# IAR MSP430工程 #
(暂时未完成)

CoAP Server 单元测试用例

# 路由说明 #
## well-known ##

## Get /hello ##
### Request ###
Header : Get (Type=CON Code=0.01)

Uri-Path : hello

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Text

Payload : "Hello CoAP"

## Get /counter ##
每次访问该路由，counter值累加
### Request ###
Header : Get (Type=CON Code=0.01)

Uri-Path : counter

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Text

Payload : "2"

## Get /json/counter ##
每次访问该路由，counter值累加,  返回格式为Application JSON
### Request ###
Header : Get (Type=CON Code=0.01)

Uri-Path : json

Uri-Path : counter

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Applicaiton-JSON

Payload : "{"value": 2}"

## Get /add?a=2&b=3 ##
Get方法 把Uri-Query中的a参数和b参数相加
### Request ###
Header : Get (Type=CON Code=0.01)

Uri-Path : add

Uri-Query : a=2

Uri-Query : b=3

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Text

Payload : "5"

## Put /add ##
Put 把Payload中的a参数和b参数相加
### Request ###
Header : PUT (Type=CON Code=0.03)

Uri-Path : add

Payload ： "a=2&b=3"

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Text

Payload : "5"

## Get /stream ##
通过Get方法获得服务器返回的二进制内容
### Request ###
Header : Get (Type=CON Code=0.02)

Uri-Path : stream

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Stream

Payload : 0xD5 0xC8

## Post /swap ##
通过Post方法把二进制内容推送至服务器，服务器把获得二进制负载倒序排列，作为响应返回至客户端
### Request ###
Header : Post (Type=CON Code=0.02)

Uri-Path : swap

Payload : 0x32 0x43 0x54

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Stream

Payload : 0x54 0x43 0x32

## Post /sort ##
通过Post方法把二进制内容推送至服务器，服务器把获得二进制负载升序排列，作为响应返回至客户端
### Request ###
Header : Post (Type=CON Code=0.02)

Uri-Path : sort

Payload : 0x12 0x45 0x32

### Response ###
Header : 2.05 Content(Type=ACK Code=2.05)

Content-Format: Stream

Payload : 0x12 0x32 0x45

# 参考 #
代码参考github 1248 microcoap

https://github.com/1248/microcoap