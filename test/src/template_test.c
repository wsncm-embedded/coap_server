#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>

TEST_GROUP(tg_template);

TEST_SETUP(tg_template)
{
    ;
}

TEST_TEAR_DOWN(tg_template)
{
    ;
}

TEST(tg_template, tc01_sprintf)
{
    char str[32];
    sprintf(str, "Hello %s\n", "CoAP");
    TEST_ASSERT_EQUAL_STRING("Hello CoAP\n", str);
}

TEST(tg_template, tc01_sscanf)
{
    char str[] = "a=2&b=3";
    int a, b;
    sscanf(str, "a=%d&b=%d", &a, &b);
    
    TEST_ASSERT_EQUAL_INT(2, a);
    TEST_ASSERT_EQUAL_INT(3, b);
}

TEST_GROUP_RUNNER(tg_template)
{
    RUN_TEST_CASE(tg_template, tc01_sprintf);
    RUN_TEST_CASE(tg_template, tc01_sscanf)
}
