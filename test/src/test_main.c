#include "unity_fixture.h"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(tg_template);    
}

const static char *argstring[] =
{
    "main",
    "-v",
};

int main(int argc, const char *argv[])
{
    argc = sizeof(argstring) / sizeof(char *);
    argv = argstring;

    return UnityMain(argc, argv, RunAllTests);
}
