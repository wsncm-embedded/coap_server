#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>

#define BUF_SIZE        1024 
#define PORT            5683            // coap 默认端口
char recv_data[BUF_SIZE];               // 接收缓冲区
char send_data[BUF_SIZE];               // 发送缓冲区

int main(int argc, char **argv)
{
    WSADATA wsaData;
    
    int ret;
    int addr_len;
    int fd;
    struct sockaddr_in server_addr, client_addr;

    // Winsows下启用socket
    ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (ret != 0) 
    {
        printf("WSAStartup failed: %d\n", ret);
        return 1;
    }

    // 创建UDP套接字
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);
    memset(&(server_addr.sin_zero),0, sizeof(server_addr.sin_zero));
    
    // 绑定
    bind(fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    
    addr_len = sizeof(struct sockaddr);
    printf("UDP Server Waiting for client on port %d...\n", PORT);

    while (1)
    {
        int recv_len = recvfrom(fd, recv_data, BUF_SIZE - 1, 0,
                             (struct sockaddr *)&client_addr, &addr_len);
        recv_data[recv_len] = '\0'; 
        
        // 打印数据
        printf("\n(%s, %d) said:", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
        printf("%s", recv_data);

        sprintf(send_data, "Hello %s\n", recv_data);
        int send_len = strlen(send_data);
        
        sendto(fd, send_data, send_len, 0, (struct sockaddr *)&client_addr, sizeof(client_addr));
    }
    
    // 关闭套接字
    closesocket(fd);
    WSACleanup();

    // 输入任何字符则关闭程序
    printf("Finish\n");
    return 0;
}
