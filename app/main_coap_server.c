#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <windows.h>
#include <stdint.h>
#include <stddef.h>

#include "coap.h"

#define BUF_SIZE        1024 
#define PORT            5683                // coap 默认端口

uint8_t request_data[BUF_SIZE];                // 接收缓冲区
uint8_t response_data[BUF_SIZE];               // 发送缓冲区

int request_len;
int response_len;

int main(int argc, char **argv)
{
    WSADATA wsaData;
    
    int ret;
    int addr_len;
    int fd;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    
    // scratch_buf 临时存放缓冲区
    uint8_t scratch_raw[128];
    coap_rw_buffer_t scratch_buf = {scratch_raw, sizeof(scratch_raw)};

    // Winsows下启用socket
    ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (ret != 0) 
    {
        printf("WSAStartup failed: %d\n", ret);
        return 1;
    }

    // 创建UDP套接字
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);
    memset(&(server_addr.sin_zero),0, sizeof(server_addr.sin_zero));
    
    // 绑定
    bind(fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    
    addr_len = sizeof(struct sockaddr);
    printf("UDP Server Waiting for client on port %d...\n", PORT);

    // 访问well-known端口使用
    endpoint_setup();

    while (1)
    {
        coap_packet_t request_pkt;
        int rc;
        
        request_len = recvfrom(fd, (char*)request_data, BUF_SIZE - 1, 0,
                             (struct sockaddr *)&client_addr, &addr_len);
        
        // 打印UDP原始内容
        printf("\n\nReceived: ");
        coap_dump(request_data, request_len);
        printf("\n");

        // coap_parse把从UDP获得的二进制请求转换为CoAP结构体
        if (0 != (rc = coap_parse(&request_pkt, request_data, request_len)))
        {
           printf("Bad packet rc=%d\n", rc); 
        }
        else
        {
            response_len = sizeof(response_data);
            coap_packet_t response_pkt;

            // 打印CoAP请求
            printf("CoAP Requset\n");
            coap_dump_packet(request_pkt);

            // 处理请求，获得response_pkt
            coap_handle_req(&scratch_buf, &request_pkt, &response_pkt);

            if (0 != (rc = coap_build(response_data, (size_t*)&response_len, &response_pkt)))
            {
                printf("coap_build failed rc=%d\n", rc);
            }
            else
            {
                // 打印原始16进制内容
                printf("\nSending: ");
                coap_dump(response_data, (size_t)response_len);
                printf("\n");

                // 打印CoAP各部分
                printf("CoAP Response\n");
                coap_dump_packet(response_pkt);
                printf("End\n\n");

                // 通过UDP发送
                sendto(fd, (char*)response_data, response_len, 0, (struct sockaddr *)&client_addr, sizeof(client_addr));
            }
        }
    }
    
    // 关闭套接字
    closesocket(fd);
    WSACleanup();

    // 输入任何字符则关闭程序
    printf("Finish\n");
    return 0;
}
